const express = require('express');
const router = express.Router();
const generic_controller = require('./genericMiddleware_controller');


router.post('/genericMethod/:id',generic_controller.genericMethodID);
router.post('/genericMethod',generic_controller.genericMethod);
router.post('/createUser',generic_controller.genericMethod);

 

module.exports = router;