const express = require('express');
const router = express.Router();
const room_controller = require('./room_controller');


router.post('/openRoom',room_controller.openRoom);

module.exports = router;