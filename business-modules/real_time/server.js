const socketIO = require('socket.io');
const http = require('http');
module.exports=(app)=>{
    
    const server = http.createServer(app);
    let numberOfOnlineUsers=0;
    const io = socketIO(server);
    io.on('connection', (socket) => {
        numberOfOnlineUsers++;
        io.emit('numberOfOnlineUsers', numberOfOnlineUsers);
        
        console.log('New user connected');
    
        socket.on('disconnect', () => {
            numberOfOnlineUsers--;
            io.emit('numberOfOnlineUsers', numberOfOnlineUsers);
            console.log('User disconnected');
        });
    });
}
