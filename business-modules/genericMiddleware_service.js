var apiRequest = require('request-promise');

module.exports = {
    sendRequest,
   
}



function sendRequest(behavior, url, body,headers) {
    return new Promise(async (resolve, reject) => {
        try {
            
            var options;
            var PostOptions = {
                method:"POST",
                uri: url,
                body: body,
                headers:headers,
                json: true 
            };
            var GetOptions = {
                method:"GET",
                uri: url,
                headers:headers,
            };
            behavior.trim().toUpperCase()==='POST'? options=PostOptions:options=GetOptions;
           
            
            
            if (options)
            apiRequest(options)
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (result) {
                    resolve(result.error)
                });
           
            

        }
        catch (error) {
            reject(error)
        }
    })
}
 