const renderResponseUtil = require('../utils/RenderResponseUtil');
const generic_service = require('./genericMiddleware_service');
module.exports = {
    genericMethod,
    genericMethodID,
    createUser
}

async function genericMethod(req, res) {
    try {
       
        const url = req.body.url;
        const body = req.body.body;
        const headers = req.body.headers;
        const behavior = req.body.behavior;
        const requestResult = await generic_service.sendRequest(behavior, url, body, headers);
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}

async function genericMethodID(req, res) {
    try {
        const id = req.params.id;
        const url = req.body.url;
        const body = req.body.body;
        const headers = req.body.headers;
        const behavior = req.body.behavior;
        const requestResult = await generic_service.sendRequest(behavior, url, body, headers);
        if (id === 'p') {
            if (requestResult.success) {
                var setTypePrivate = await generic_service.sendRequest(behavior, "http://41.39.63.112:3000/api/v1/channels.setType", { roomId: requestResult.channel._id, type: "p" }, headers);
                renderResponseUtil.sendResponse(req, res, setTypePrivate)
            }else  renderResponseUtil.sendResponse(req, res, requestResult)
        }else  renderResponseUtil.sendResponse(req, res, requestResult)

    } catch (error) {
        res.send(error);
    }

}
async function createUser(req, res) {
    try {
        const url = req.body.url;
        const body = req.body.body;
        const headers = req.body.headers;
        const behavior = req.body.behavior;
        const requestResult = await generic_service.sendRequest(behavior, url, body, headers);
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}