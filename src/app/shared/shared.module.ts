import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsocketService } from './websocket.service';
import { ChatService } from './chat.service';
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers:[WebsocketService,ChatService]
})
export class SharedModule { }
