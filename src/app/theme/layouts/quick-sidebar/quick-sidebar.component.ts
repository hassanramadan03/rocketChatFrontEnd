import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../helpers';
import { AuthenticationService } from "../../../auth/_services/authentication.service";
import { ChatService } from "../../../shared/chat.service";
@Component({
    selector: "app-quick-sidebar",
    templateUrl: "./quick-sidebar.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class QuickSidebarComponent implements OnInit {
    userDetails: any;
    msg='';
    constructor(private authService: AuthenticationService,private chat: ChatService) {

    }

    ngOnInit() {
        this.chat.messages.subscribe(msg => {
            console.log(msg);
            
          })
        this.userDetails = this.authService.getCurentUser();
    }
    sendMessage() {
        console.log(this.msg)
        this.chat.sendMsg(this.msg);
      }
      
}